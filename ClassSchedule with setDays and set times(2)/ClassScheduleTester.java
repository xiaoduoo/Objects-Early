public class ClassScheduleTester
{
   public static void main(String[] args)
   {
      ClassSchedule class1 = new ClassSchedule("Intro to Java","MWF","9:00", "9:50","Turing 101");

      // your work here - test the getTime method
      System.out.println(class1.getTime());
      System.out.println("Expected: "+class1.getTime());

      ClassSchedule class2 = new ClassSchedule("Operating Systems","TTh","12:30","13:45","Turing 307"); 
      // your work here - test the setStartTime method
      class2.setStartTime("13:00");
      System.out.println(class2.getTime());
      System.out.println("Expected: "+class2.getTime());
   }
}