public class PasswordTester
{
   public static void main(String[] args)
   {
      String originalPassword="asecretid";
      String modifiedPassword="";
      for(int i=0;i<originalPassword.length();i++){
         if(originalPassword.charAt(i)=='e'){
            modifiedPassword+='3';
         }
         else if(originalPassword.charAt(i)=='i'){
            modifiedPassword+='1';
         }
         else if(originalPassword.charAt(i)=='a'){
            modifiedPassword+='@';
         }
         else if(originalPassword.charAt(i)=='s'){
            modifiedPassword+='$';
         }else{
            modifiedPassword+=originalPassword.charAt(i);
         }
      }
            
      System.out.println(modifiedPassword);
      // Also print out what you expect
      System.out.println("Expected: "+modifiedPassword);
   }
}