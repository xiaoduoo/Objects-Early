public class UppercaseTester
{
   public static void main(String[] args)
   {
      String lower1 = "hello";            
      String upper1 = lower1.toUpperCase();
      
      System.out.println(upper1);
      System.out.println("Expected: HELLO");
      
      // Now compute and print the uppercase versions
      // of the strings San José and Fuß. Also print the
      // expected values.
      String lower2 = "San José";            
      String upper2 = lower2.toUpperCase();
      
      System.out.println(upper2);
      System.out.println("Expected: "+upper2);
      
      String lower3 = "Fuß";            
      String upper3 = lower3.toUpperCase();
      
      System.out.println(upper3);
      System.out.println("Expected: "+upper3);
   }
}