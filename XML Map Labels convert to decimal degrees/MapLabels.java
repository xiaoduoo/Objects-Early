/**
   A map label has a place name, longitude and latitude
*/
public class MapLabels
{  
   private String placeName;
   private double latitude;
   private double longitude;

   /**
      Constructs a map label with place name, logitude and latitude given in decimal degrees
   */
   public MapLabels (String name, double aLat, double aLong)
   {   
      // your work here
      placeName = name;
      latitude = aLat;
      longitude = aLong;
   }
   
   /**
      Constructs a map label with place name, logitude and latitude given in degrees, minutes, seconds
   */
   public MapLabels (String name, int degLat, int minLat, double secLat, int degLong, int minLong, double secLong)   
   {   
      // your work here
      placeName = name;
      // decimalDegress = Math.signum(degrees) * (Math.abs(degrees) + minutes/60. + seconds/3600.);
      latitude = Math.signum(degLat) * (Math.abs(degLat) + minLat/60. + secLat/3600.);
      longitude = Math.signum(degLong) * (Math.abs(degLong) + minLong/60. + secLong/3600.);
   }
   
   /**
      Gets the string in XML form
      @return the string
   */
   public String toString()
   {   
      //your work here
      // use \" to print a "
      String label = "<label name=\"" + placeName + "\" latitude=\""+latitude+"\" longitude=\""+longitude+"\"/>";

      return label;
   }   
}