// your work here
import java.awt.Rectangle;

public class AreaTester
{
   public static void main(String[] args)
   {
      // your work here
      Rectangle rect = new Rectangle(0,0,17,3);
      double area = rect.getWidth() * rect.getHeight();
      System.out.println(area);
      System.out.println("Expected: 51");
   }
}