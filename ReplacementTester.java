public class ReplacementTester
{
   public static void main(String[] args)
   {
      String greeting = "Hello, elite hacker!";
      String modifiedGreeting = "H3770, 371t3 hack3r!";
      System.out.println(modifiedGreeting);
      System.out.println("Expected: H3770, 371t3 hack3r!");
   }
}